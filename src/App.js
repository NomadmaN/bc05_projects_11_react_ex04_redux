import logo from './logo.svg';
import './App.css';
import ShoesStoreReducer from './ShoesStore/ShoesStoreReducer';

function App() {
  return (
    <div className="App">
      <ShoesStoreReducer></ShoesStoreReducer>
    </div>
  );
}

export default App;

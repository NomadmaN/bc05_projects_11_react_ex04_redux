import {products} from '../../productsData'

let initialState = {
    productData: products,
    productDetail: products[0],
    productCart: [],
}

export const shoeReducer = (state = initialState, action) => {
    switch (action.type){
        case 'ADD_TO_CART': {
            let clonedCart = [...state.productCart];
            let clonedIndex = clonedCart.findIndex((item) => {
                return item.id === action.payload.id
            })
            // call cartItem as an object, adding 1 more key number
            // action.payload like param item
            let cartItem = {...action.payload, number: 1}
            if (clonedIndex === -1){
                clonedCart.push(cartItem);
            }else{
                clonedCart[clonedIndex].number += 1;
            }
            // update state for redering
            return {...state, productCart: clonedCart}
        }
        case 'VIEW_DETAIL': {
            return {...state, productDetail: action.payload}
        }
        case 'ADJUST_QUANTITY': {
            const {index, adjustment} = action;
            let clonedCart = [...state.productCart];
            if (adjustment === true){
                clonedCart[index].number += 1;
            }else{
                if (clonedCart[index].number > 1){
                    clonedCart[index].number -= 1;   
                }
            }
            return {...state, productCart: clonedCart}
        }
        case 'REMOVE_ITEM': {
            let clonedCart = [...state.productCart];
            clonedCart.splice(action.index, 1);
            // update state for redering
            return {...state, productCart: clonedCart}
        }
        default:
            return state
    }
}
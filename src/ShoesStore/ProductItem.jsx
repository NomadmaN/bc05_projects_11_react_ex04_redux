import React, { Component } from 'react'
import { connect } from 'react-redux';

class ProductItem extends Component {
  render() {
    // OOP let {object} = dataShoes defined in parent class for <ShoesItem> component
    let {image,name} = this.props.dataProduct;
    return (
        // Create this div to make spacing between card using padding-1
        <div className='col-3 p-1'>
            {/* create below card from b4-card-align command */}
            <div className="card text-left">
                <img className="card-img-top" src={image} alt="" />
                <div className="card-body">
                    <h6 className="card-title">{name}</h6>
                    <button 
                        onClick={()=>{
                            // update function defined below to link to ShoesStoreReducer
                            this.props.handleAddToCart(this.props.dataProduct)
                        }} className='btn btn-success' data-toggle="modal" data-target="#modelCartId">Add to Cart</button>
                    <button 
                        onClick={()=>{
                            this.props.handleViewDetail(this.props.dataProduct)
                        }} className='btn btn-primary ml-2' data-toggle="modal" data-target="#modelId">Detail</button>
                </div>
            </div>
        </div>
    )
  }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAddToCart: (value) => {
            dispatch({
                type: 'ADD_TO_CART',
                payload: value,
            })
        },
        handleViewDetail: (value) => {
            dispatch({
                type: 'VIEW_DETAIL',
                payload: value,
            })
        },
    }
}

// connect ProductItem to ShoesStoreReducer
// NOTE: when a component has no state to link like this case, we need to define null before mapDispatchToProps in connect()
export default connect(null,mapDispatchToProps)(ProductItem)
import React, { Component } from 'react'
import ProductList from './ProductList'
import ModalDetail from './ModalDetail'
import ModalCart from './ModalCart'

export default class ShoesStoreReducer extends Component {
    state = {

    }

  render() {
    return (
      <div className='container'>
        <h3 className='py-2' style={{backgroundColor:'#F3EFE0', fontSize: '40px'}}>Shoes Shop - Redux</h3>
        {/* shoes cart */}
        <ModalCart></ModalCart>
        {/* list shoes */}
        <ProductList></ProductList>
        {/* shoes detail */}
        <ModalDetail></ModalDetail>

      </div>
    )
  }
}

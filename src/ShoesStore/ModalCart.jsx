import React, { Component } from 'react'
import { connect } from 'react-redux'

class ModalCart extends Component {
    renderTBody= () => {
        // update cart to link with shoeReducer
        return this.props.shoppingCart.map((item, index) => {
            // always define key={index} at the start of function's cover selector
            return <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.name}</td>
                {/* set price = price * quantity */}
                <td className="align-middle">${item.price * item.number}</td>
                <td className='d-flex justify-content-center align-items-center'>
                    {/* update function as defined below on mapDispatchToProps */}
                    <div onClick={()=>this.props.handleAdjustQuantity(index,false)} className='subQty '>-</div>
                    {item.number}
                    {/* update function as defined below on mapDispatchToProps */}
                <div onClick={()=>this.props.handleAdjustQuantity(index,true)} className='addQty'>+</div>
                </td>
                <td className="align-middle">
                    <img style={{width: '80px'}} src={item.image} alt="" />
                </td>
                <td className="align-middle"><div onClick={()=>this.props.handleRemoveItem(index)} className='removeItem' title="Remove this item">X</div></td>
            </tr>
        })
    }
  render() {
    return (
        <div>
            {/* Modal */}
            <div className="modal fade" id="modelCartId" tabIndex={-1} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{width: '800px', maxWidth: '800px'}}>
                        <div className="modal-header">
                            <h5 className="modal-title" style={{fontWeight: 'bold', fontSize: '30px'}}>PRODUCT CART</h5>
                            <button type="button" className="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <table className='table'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Image</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderTBody()}
                                </tbody>
                                <tfoot>
                                    <tr className='text-danger' style={{fontSize: '20px', fontWeight: 'bold'}}>
                                        <td colSpan='1'></td>
                                        <td>Total Cost:</td>
                                        {/* using update defined below */}
                                        <td>${this.props.shoppingCart.reduce((totalCost,item,index) => {return totalCost += Number(item.number * item.price)},0)}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">Purchase</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

let mapStateToProps = (state) => {
    return {
        shoppingCart: state.shoeReducer.productCart,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAdjustQuantity: (index, adjustment) => {
            const action = {
                type: 'ADJUST_QUANTITY',
                index,
                adjustment,
            }
            dispatch (action);
        },
        handleRemoveItem: (index) => {
            const action = {
                type: 'REMOVE_ITEM',
                index,
            }
            dispatch (action);
        }, 
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ModalCart)